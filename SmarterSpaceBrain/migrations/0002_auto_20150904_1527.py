# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SmarterSpaceBrain', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='spaceuser',
            name='membertype',
            field=models.CharField(max_length=1, choices=[(b'F', b'Full member'), (b'R', b'Reduction member'), (b'N', b'No member')]),
        ),
    ]
