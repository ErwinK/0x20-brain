from django.contrib import admin
from SmarterSpaceBrain.models import SpaceUser
from gatekeeper.models import Badge, Phonenumber, GatekeeperSchedule

# Register your models here.

class BadgeAdmin(admin.ModelAdmin):
    list_display = ['number', 'user_id']

admin.site.register(Badge, BadgeAdmin)

class GatekeeperScheduleAdmin(admin.ModelAdmin):
    list_display = ['day', 'starttime', 'endtime']

admin.site.register(GatekeeperSchedule, GatekeeperScheduleAdmin)


class PhonenumbersAdmin(admin.ModelAdmin):

    list_display = ['phonenumber', 'user_id']

admin.site.register(Phonenumber, PhonenumbersAdmin)