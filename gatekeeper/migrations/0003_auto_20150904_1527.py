# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gatekeeper', '0002_phonenumbers'),
    ]

    operations = [
        migrations.CreateModel(
            name='Badge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=12)),
                ('own', models.BooleanField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Phonenumber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phonenumber', models.TextField()),
                ('cellphone', models.BooleanField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RenameModel(
            old_name='GatekeeperSchedules',
            new_name='GatekeeperSchedule',
        ),
        migrations.RemoveField(
            model_name='badges',
            name='user',
        ),
        migrations.RemoveField(
            model_name='phonenumbers',
            name='user',
        ),
        migrations.DeleteModel(
            name='Badges',
        ),
        migrations.DeleteModel(
            name='Phonenumbers',
        ),
    ]
