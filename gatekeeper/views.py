from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from gatekeeper.models import Badge, Phonenumber
from gatekeeper.serializers import BadgeSerializer, PhonenumberSerializer

# Create your views here.

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


def badges_list(request):
    """Lists all badges"""
    if request.method == 'GET':
        badges = Badge.objects.all()
        serializer = BadgeSerializer(badges, many=True)
        return JSONResponse(serializer.data)

def phonenumbers_allowed(request):
    """Lists all phonenumbers that are allowed to open the gate"""
    if request.method == 'GET':
        badges = Phonenumber.objects.filter(user__membertype = "F" or "R")
        serializer = PhonenumberSerializer(badges, many=True)
        return JSONResponse(serializer.data)
