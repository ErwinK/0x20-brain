from rest_framework import serializers
from gatekeeper.models import Badge, Phonenumber

class BadgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Badge

# Serializers define the API representation.
class PhonenumberSerializer(serializers.ModelSerializer):



    class Meta:
        model = Phonenumber
        fields = ('user_id', 'phonenumber', 'cellphone')
